<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix("v1")->group(function() {
    // login
    Route::post("login", "UserController@login");
    Route::post('register', 'UserController@createUser');

    // middleware error: auth, role
    Route::prefix("error")->group(function () {
        Route::get("auth-error", "UserController@authFailed")->name('api-auth-failed');
        Route::get("role-error", "UserController@roleFailed")->name('api-auth-failed');
    });

    // URL to get image
    Route::get("image/{userId}/{fileName}", "FileController@accessImage");

    // API with auth (role) check
    Route::middleware(['auth:api', 'role:both'])->group(function () {
        Route::prefix("user")->group(function () {
            Route::post('/', 'UserController@accessUser');
            Route::post('/all', 'UserController@accessUsers')->middleware('role:manager');
            Route::post('/create', 'UserController@createManager')->middleware('role:manager');
            Route::post('/update', 'UserController@updateUser');
            Route::post('/update-image', 'UserController@updateUserImage');
            Route::post('/update-pwd', 'UserController@updatePassword');
            Route::post('/delete', 'UserController@removeUser');
            Route::post('/logout', 'UserController@logout');
        });

        Route::prefix("stock-data")->group(function () {
            Route::post("/upload", "StockDataController@upload")->middleware('role:manager');
            Route::post("/data", "StockDataController@accessByStockCode");
        });

        Route::prefix("company")->group(function () {
            Route::post("/all", "CompanyController@getAll");
            Route::post("/create", "CompanyController@addCompany")->middleware('role:manager');
            Route::post("/update", "CompanyController@updateCompany")->middleware('role:manager');
            Route::post("/remove", "CompanyController@removeCompany")->middleware('role:manager');
            Route::post("/create/keyword", "CompanyController@addCompanyKeyword")->middleware('role:manager');
            Route::post("/remove/keyword", "CompanyController@removeCompanyKeyword")->middleware('role:manager');
            Route::post("/create/user-tag", "UserDailyTagController@addNewDailyTag")->middleware('role:user');
            Route::post("/remove/user-tag", "UserDailyTagController@removeDailyTag");
        });

        Route::prefix("daily")->group(function () {
            Route::post("/news", "CompanyNewsController@accessDailyNews");
            Route::post("/company/news", "CompanyNewsController@accessDailyCompanyNews");
            Route::post("/company/user-tag", "UserDailyTagController@accessDailyTag");
        });

        Route::prefix("news")->group(function () {
            Route::post("/crawled", "CompanyNewsController@crawledNews")->middleware('role:manager');
            Route::post("/classify", "CompanyNewsController@classifyNews")->middleware('role:manager');
            Route::post("/classify-with-date", "CompanyNewsController@classifyNewsByTimestamp")->middleware('role:manager');
        });

        Route::prefix("note")->group(function () {
            Route::post("/", "NoteController@accessNoteById");
            Route::post("/all", "NoteController@accessAllNotes");
            Route::post("/create", "NoteController@createNote")->middleware('role:user');
            Route::post("/update", "NoteController@updateNote")->middleware('role:user');
            Route::post("/delete", "NoteController@deleteNote");
        });
    });
});
