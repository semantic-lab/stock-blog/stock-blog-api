<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_stocks', function (Blueprint $table) {
            $zero = 0;
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->bigInteger('timestamp');
            $table->string('date');
            $table->float('opening_price');
            $table->float('daily_max_price');
            $table->float('daily_min_price');
            $table->float('closing_price');
            $table->integer('volume');
            $table->float('avg_price1');
            $table->float('avg_price5')->default($zero);
            $table->float('avg_price10')->default($zero);
            $table->float('avg_price20')->default($zero);
            $table->float('avg_price60')->default($zero);
            $table->float('avg_price120')->default($zero);
            $table->float('avg_price240')->default($zero);
            $table->integer('avg_volume5')->default($zero);
            $table->integer('avg_volume20')->default($zero);
            $table->integer('avg_volume60')->default($zero);
            $table->integer('avg_volume120')->default($zero);
            $table->float('macd_bar')->default($zero);
            $table->float('ks933')->default($zero);
            $table->float('ds933')->default($zero);
            $table->float('price_change_value')->default($zero);
            $table->string('price_change_type');
            $table->string('candle_stick_color');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_stocks');
    }
}
