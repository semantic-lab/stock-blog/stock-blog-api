<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('timestamp');
            $table->bigInteger('company_id')->unsigned();
            $table->bigInteger('news_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('news_id')->references('id')->on('news');
            $table->unique([ 'company_id', 'news_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_news');
    }
}
