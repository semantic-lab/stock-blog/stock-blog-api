# stock-blog-api
基於 Laravel 的 stock-blog API server

此 API server 使用的框架如下:
01. MVC 架構 (前端 view 獨立為 nuxt.js 專案)
02. Laravel 5.8
03. docker (laradock) 模式部屬
04. MongoDB


## API & 相關功能
* 系統資料如下:
    01. 台灣 50 個股資料 (yyyy-MM-dd - )
    02. 台股指數
    03. 美國指數
    04. 使用者
        * 基本資料
        * 個股提醒點
    05. 筆記

* API:
    01. 存取股市資料
        * 資料轉置
        * 即時資料
        * 技術指標運算 (人工)
    02. 存取使用者資料
    03. 存取筆記資料