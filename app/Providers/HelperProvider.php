<?php

namespace App\Providers;

use App\helpers\ExceptionHelper;
use App\helpers\Helper;
use Illuminate\Support\ServiceProvider;

class HelperProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Helper::class, function () {
            return new Helper();
        });

        $this->app->singleton(ExceptionHelper::class, function () {
            return new ExceptionHelper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
