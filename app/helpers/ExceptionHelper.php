<?php


namespace App\helpers;


use Throwable;

class ExceptionHelper
{
    private $errorList = [
        // sql error:
        'sqlDuplicateError' => [
            'message' => 'sql create duplicate record error (23000)',
            'code' => 23000,
        ],
        'sqlColumnUndefinedError' => [
            'message' => 'sql query on undefined column (sql: 42S22 / api: 420022 )',
            'code' => 420022,
        ],

        // repository error: 1000 - 1999:
        'initModelError' => [
            'message' => 'repository initial model error (1000)',
            'code' => 1000,
        ],
        'createModelError' => [
            'message' => 'repository create new record error (1001)',
            'code' => 1001,
        ],
        'selectNoResultError' => [
            'message' => 'repository select no record error (1002)',
            'code' => 1002,
        ],
        'deleteModelError' => [
            'message' => 'repository remove record error (1003)',
            'code' => 1003,
        ],
        'updateModelError' => [
            'message' => 'repository update record error (1004)',
            'code' => 1004,
        ],

        // service error: 2000 - 2999:
        'transModelInCamelCaseError' => [
            'message' => 'service trans model in camel case error (2000)',
            'code' => 2000,
        ],
        'transModelCollectionInCamelCaseError' => [
            'message' => 'service trans collection of models in camel case error (2001)',
            'code' => 2001,
        ],
        'actionWithoutPermissionError' => [
            'message' => 'service action without permission (2002)',
            'code' => 2002,
        ],

        // auth error: 3000 - 3999:
        'loginError' => [
            'message' => 'email or password error for login',
            'code' => 3000,
        ],
        'tokenError' => [
            'message' => 'auth token error, no permission to using API',
            'code' => 3001
        ],

    ];

    private $errorListByCode = [];

    public function __construct()
    {
        foreach ($this->errorList as $name => $value) {
            array_push($this->errorListByCode, $value);
        }
    }

    public function getExceptionByName($name)
    {
        if ($this->hasExceptionName($name)) {
            $info = $this->errorList[$name];
            return new \Exception($info['message'], $info['code']);
        }
        return null;
    }

    public function getExceptionByCode($code)
    {
        if ($this->hasExceptionCode($code)) {
            $info = $this->errorListByCode[$code];
            return new \Exception($info['message'], $info['code']);
        }
        return null;
    }

    public function hasExceptionName($name)
    {
        return array_key_exists($name, $this->errorList);
    }

    public function hasExceptionCode($code)
    {
        return array_key_exists((int)$code, $this->errorListByCode);
    }
}
