<?php


namespace App\helpers;


class PageInfo
{
    public $at;
    public $size;
    public $offset;
    private $defaultAt = 1;
    private $defaultSize = 10;

    public function set($data)
    {
        $atError = false;
        try {
            $this->at = $data['at'];
        } catch (\Exception $ex) {
            $atError = true;
            $this->at = $this->defaultAt;
        }

        $sizeError = false;
        try {
            $this->size = $data['size'];
        } catch (\Exception $ex) {
            $sizeError = true;
            $this->size = $this->defaultSize;
        }

        $this->calculateOffset();

        return ($atError && $sizeError) ? null : $this;
    }

    private function calculateOffset()
    {
        $this->offset = ($this->at - 1) * $this->size;
    }
}
