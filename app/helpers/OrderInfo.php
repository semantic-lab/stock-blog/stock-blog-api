<?php


namespace App\helpers;


class OrderInfo
{
    public $column;
    public $orientation;
    private $defaultColumn = 'id';
    private $defaultOrientation = 'asc';

    public function set($data)
    {
        $columnError = false;
        try {
            $this->column = $data['column'];
        } catch (\Exception $ex) {
            $columnError = true;
            $this->column = $this->defaultColumn;
        }

        $orientationError = false;
        try {
            $this->orientation = $data['orientation'];
        } catch (\Exception $ex) {
            $orientationError = true;
            $this->orientation = $this->defaultOrientation;
        }

        return ($columnError && $orientationError) ? null : $this;
    }
}
