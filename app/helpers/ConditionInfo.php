<?php


namespace App\helpers;


use Illuminate\Foundation\Http\FormRequest;

class ConditionInfo
{
    /*
    "consditions": [{
        "column": "required|string|news.title,company.name,...",
        "value": "required",
    }]
    */

    private $whereConditions = [];
    private $withConditions = [];

    public function set(FormRequest $request, bool $usingLikeOperation = false)
    {
        $conditionsInRequest = $request['conditions'];
        if ($conditionsInRequest) {
            foreach ($conditionsInRequest as $condition) {
                $column = $condition['column'];
                $value = $condition['value'];
                $valueIsSet = isset($value);
                if ($valueIsSet) {
                    $isForeignColumn = strpos($column, '.') !== false;
                    if ($isForeignColumn) {
                        $columnInfo = explode(".", $column);
                        $foreignPropInModel = $columnInfo[0];
                        $foreignColumn = $columnInfo[1];
                        if (!array_key_exists($foreignPropInModel, $this->withConditions)) {
                            $this->withConditions[$foreignPropInModel] = [];
                        }
                        array_push(
                            $this->withConditions[$foreignPropInModel],
                            $this->generateSingleCondition($foreignColumn, $usingLikeOperation, $value)
                        );
                    } else {
                        array_push(
                            $this->whereConditions,
                            $this->generateSingleCondition($column, $usingLikeOperation, $value)
                        );
                    }
                }
            }
        }

        return $this;
    }

    public function integrateConditions(array &$conditions)
    {
        $conditions = array_merge($conditions, $this->whereConditions);
        $conditions['with'] = $this->withConditions;
        return $this;
    }

    private function generateSingleCondition($conditionColumn, $usingLikeOperation, $value) {
        $conditionOperator = ($usingLikeOperation) ? "like" : "=";
        $conditionValue = ($usingLikeOperation) ? "%{$value}%" : $value;
        return [ $conditionColumn, $conditionOperator, $conditionValue ];
    }
}
