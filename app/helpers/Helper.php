<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/15
 * Time: 上午 11:28
 */

namespace App\helpers;

use App\Http\Responses\APIResponse;
use App\Services\CompanyService;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;


class Helper
{
    private $exceptionHelper;

    public function __construct(ExceptionHelper $exceptionHelper)
    {
        $this->exceptionHelper = $exceptionHelper;
    }

    public function setFailedAPIResponseWithException(APIResponse &$response, \Exception $exception, array $skipExceptionCodes = [])
    {
        $errorMessage = $exception->getMessage();
        $errorCode = $exception->getCode();
        $toSkip = in_array((int)$errorCode, $skipExceptionCodes);
        if ($toSkip) {
            $response->isSuccess();
        } else {
            $response->isFailed()
                ->setCode($errorCode)
                ->setMessage($errorMessage)
                ->setHttpStatus(550);
        }
    }

    public function jsTimestampOfTaiwan(string $date, string $format = "!Y/m/d")
    {
        return Carbon::createFromFormat($format, $date)->getTimestamp() * 1000 - 28800000;
    }

    public function initRequestField(FormRequest $request, string $fieldName, $defaultValue = null)
    {
        $isExist = $request->has($fieldName);
        if ($isExist) {
            $value = $request[$fieldName];
            $notNullValue =  !is_null($value) && isset($value) && !empty($value);
            if ($notNullValue) {
                return $value;
            }
        }
        return $defaultValue;
    }
}
