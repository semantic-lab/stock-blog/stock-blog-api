<?php


namespace App\helpers;


class ResponseCode
{
    public const SUCCESS = 200;

    public const GENERAL_FAILED = 500;

    public const AUTH_FAILED = 501;

    public const ROLE_FAILED = 502;
}
