<?php

namespace App\Models;

class Keyword extends BaseModel
{
    protected $table = "keywords";

    public $timestamps = false;

    public function companies() {
        return $this->belongsTo('App\Models\Company');
    }
}
