<?php

namespace App\Models;

class DailyStock extends BaseModel
{
    protected $table = "daily_stocks";

    public $timestamps = false;
}
