<?php

namespace App\Models;

class UserDailyTag extends BaseModel
{
    protected $table = "user_daily_tags";

    public $timestamps = false;

    public function tag()
    {
        return $this->belongsTo('App\Models\Tag');
    }
}
