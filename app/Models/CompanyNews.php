<?php

namespace App\Models;

class CompanyNews extends BaseModel
{
    protected $table = "company_news";

    public $timestamps = false;

    public function news()
    {
        return $this->belongsTo('App\Models\News');
    }

    public function companies()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
}
