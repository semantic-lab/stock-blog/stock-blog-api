<?php

namespace App\Models;

class Tag extends BaseModel
{
    protected $table = "tags";

    public $timestamps = false;
}
