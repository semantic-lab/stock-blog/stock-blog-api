<?php

namespace App\Models;

class Company extends BaseModel
{
    protected $table = "companies";

    public function keywords() {
        return $this->hasMany('App\Models\Keyword');
    }
}
