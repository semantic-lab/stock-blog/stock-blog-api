<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/14
 * Time: 下午 02:33
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BaseModel extends Model
{
    public function getAttribute($key)
    {
        return parent::getAttribute(Str::snake($key));
    }

    public function setAttribute($key, $value)
    {
        parent::setAttribute(Str::snake($key), $value);
    }
}