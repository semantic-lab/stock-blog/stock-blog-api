<?php

namespace App\Models;

class News extends BaseModel
{
    protected $table = "news";

    public $timestamps = false;
}
