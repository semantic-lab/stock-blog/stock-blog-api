<?php

namespace App\Http\Middleware;

use App\Http\Responses\APIResponse;
use Closure;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\ParameterBag;

class XSSFilterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string $type - encode, exception, pass
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        $requestBody = $request->request->all();
        $queryStrings = $request->query->all();
        $requestXSSIsPossible = $this->checkXSS($requestBody, $type, $request);
        $queryXSSIsPossible = $this->checkXSS($queryStrings, $type, $queryStrings);

        if ($queryXSSIsPossible && $type === "encode") {
            $newParamsBag = new ParameterBag($queryStrings);
            $request->query = $newParamsBag;
        }


        if ($requestXSSIsPossible || $queryXSSIsPossible) {
            switch ($type) {
                default:
                case "exception":
                    $response = new APIResponse();
                    $response->isFailed()->setMessage("Request data or query string can not include HTML special characters");
                    throw new HttpResponseException($response->toJSONResponse());
                case "pass":
                case "encode":
                    break;
            }
        }
        return $next($request);
    }

    private function checkXSS(array $data, $type, &$target)
    {
        $result = false;
        foreach ($data as $field => $value) {
            $hasHTMLChar = preg_match("/<.*?>|<\/.*?>/", $value);
            if ($hasHTMLChar) {
                $result = true;
                if ($type === "encode") {
                    $encodeResult = htmlspecialchars($value);
                    $target[$field] = $encodeResult;
                } else if ($type === "exception") {
                    break;
                }
            }
        }
        return $result;
    }
}
