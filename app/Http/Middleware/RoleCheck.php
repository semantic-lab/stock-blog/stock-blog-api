<?php

namespace App\Http\Middleware;

use App\helpers\ExceptionHelper;
use App\helpers\Helper;
use App\Http\Responses\APIResponse;
use Closure;

class RoleCheck
{

    private $helper;
    private $exceptionHelper;
    private $response;

    public function __construct(Helper $helper, ExceptionHelper $exceptionHelper, APIResponse $response)
    {
        $this->helper = $helper;
        $this->exceptionHelper = $exceptionHelper;
        $this->response = $response;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param String $role
     * @return mixed
     */
    public function handle($request, Closure $next, string $role)
    {
        $user = auth()->user();
        $correctRole = false;
        $isDev = $user->isDev();
        $isManager = $user->isManager();
        $isUser = $user->isUser();
        switch ($role) {
            case "both":
                $correctRole = $isDev || $isManager || $isUser;
                break;
            case "manager":
                $correctRole = $isDev || $isManager;
                break;
            case "user":
                $correctRole = $isUser;
                break;
        }
        if ($correctRole) {
            return $next($request);
        }
        $exception = $this->exceptionHelper->getExceptionByName('tokenError');
        $this->helper->setFailedAPIResponseWithException($this->response, $exception);
        return $this->response->toJSONResponse();
    }
}
