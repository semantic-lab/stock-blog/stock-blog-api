<?php

namespace App\Http\Responses;


class ValidatorResponse implements IValidatorResponse
{
    private $code;

    private $status;

    private $data;

    private $message;

    public function __constructor($code = 200, $status = true, $data = null, $message = null)
    {
        $this->code = $code;
        $this->status = $status;
        $this->data = $data;
        $this->message = $message;
    }

    public function setCode($code) {
        if (!is_null($code)) {
            $this->code = $code;
        }
    }

    public function setStatus($status) {
        if (!is_null($status)) {
            $this->status = $status;
        }
    }

    public function setData($data) {
        if (!is_null($data)) {
            $this->data = $data;
        }
    }

    public function setMessage($message) {
        if (!is_null($message)) {
            $this->message = $message;
        }
    }

    /**
     * Update properties for FormRequest validation error
     *
     * @param \Illuminate\Support\MessageBag $error
     */
    public function setValidationError($error) {
        $this->setStatus(false);
        $this->setData($error);
    }

    /**
     * Get an array with properties
     *
     * @return array
     */
    public function toArray() {
        return [
            "code" => $this->code,
            "status" => $this->status,
            "data" => $this->data,
            "message" => $this->message,
        ];
    }

    /**
     * Get a JsonResponse with properties
     *
     * @param int $httpStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function toJSONResponse($httpStatus = 422) {
        return response()->json($this->toArray(), $httpStatus);
    }
}