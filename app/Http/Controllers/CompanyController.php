<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/13
 * Time: 下午 04:33
 */

namespace App\Http\Controllers;


use App\helpers\Helper;
use App\Http\Requests\Company\AddCompanyKeywordRequest;
use App\Http\Requests\Company\AddCompanyRequest;
use App\Http\Requests\Company\GetAllCompanyRequest;
use App\Http\Requests\Company\RemoveCompanyKeywordRequest;
use App\Http\Requests\Company\RemoveCompanyRequest;
use App\Http\Requests\Company\UpdateCompanyRequest;
use App\Http\Responses\APIResponse;
use App\Services\CompanyService;
use App\Services\KeywordService;
use App\Services\DailyStockService;
use App\helpers\PageInfo;
use App\helpers\OrderInfo;

class CompanyController extends Controller
{
    private $companyService;
    private $keywordService;
    private $dailyStockService;
    private $helper;

    public function __construct(CompanyService $companyService, KeywordService $keywordService, DailyStockService $dailyStockService, Helper $helper)
    {
        $this->companyService = $companyService;
        $this->keywordService = $keywordService;
        $this->dailyStockService = $dailyStockService;
        $this->helper = $helper;
    }

    // API

    public function getAll(GetAllCompanyRequest $request, PageInfo $stockPageInfo, OrderInfo $stockOrderInfo, APIResponse $response)
    {
        $stockPageInfo->set([ 'at' => 0, 'size' => 1 ]);
        $stockOrderInfo->set([ 'column' => 'timestamp', 'orientation' => 'desc' ]);

        try {
            // get all companies
            $companyCollection = $this->companyService->search();
            // trans array into dictionary
            $companiesInDictionary = $this->companyService->toDictionary($companyCollection);
            
            // get latest stock data
            foreach ($companiesInDictionary as $key => $company) {
                $compamy = json_decode(json_encode($company), true);
                $condition = [
                    ["company_id", "=", $company['id']]
                ];
                $stockDataCollection = $this->dailyStockService->search($condition, $stockPageInfo, $stockOrderInfo);
                $hasStockData = count($stockDataCollection);
                if ($hasStockData) {
                    $compamy['stockData'] = $this->dailyStockService->modelInCamelCase($stockDataCollection[0]);
                }
                $companiesInDictionary[$key] = $compamy;
            }
            $response->isSuccess()->setData($companiesInDictionary);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function addCompany(AddCompanyRequest $request, APIResponse $response)
    {
        // data preprocess
        $newCompanyData = [
            "name" => $request["name"],
            "stock_code" => $request["stockCode"],
        ];
        // create new company
        try {
            $newCompany = $this->companyService->create($newCompanyData);
            $newCompany = $this->companyService->modelInCamelCase($newCompany);
            $response->isSuccess()->setData($newCompany);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function updateCompany(UpdateCompanyRequest $request, APIResponse $response) {
        // data preprocess
        $updatedData = [
            "name" => $request["name"],
            "stock_code" => $request["stockCode"],
        ];
        // update exist company
        try {
            $updatedCompany = $this->companyService->update($request["companyId"], $updatedData);
            $updatedCompany = $this->companyService->modelInCamelCase($updatedCompany);
            $response->isSuccess()->setData($updatedCompany);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function removeCompany(RemoveCompanyRequest $request, APIResponse $response) {
        // remove company
        try {
            $this->companyService->delete($request["companyId"]);
            $response->isSuccess();
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function addCompanyKeyword(AddCompanyKeywordRequest $request, APIResponse $response)
    {
        // data
        $companyId = $request['companyId'];
        $newKeywordData = [
            "company_id" => $companyId,
            "name" => $request['keyword']
        ];

        // create keywords
        try {
            $this->keywordService->create($newKeywordData);
            // get the company
            $company = $this->companyService->searchById($companyId);
            $company = $this->companyService->modelInCamelCase($company);
            $response->isSuccess()->setData($company);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function removeCompanyKeyword(RemoveCompanyKeywordRequest $request, APIResponse $response)
    {
        try {
            $this->keywordService->delete($request['keywordId']);
            $response->isSuccess();
        } catch(\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }
}
