<?php


namespace App\Http\Controllers;


use App\helpers\Helper;
use App\Http\Requests\StockData\AccessByStockCodeRequest;
use App\Http\Requests\StockData\UploadRequest;
use App\Http\Responses\APIResponse;
use App\Services\CompanyService;
use App\Services\DailyStockService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StockDataController extends Controller
{
    private $companyService;
    private $dailyStockService;
    private $helper;

    public function __construct(CompanyService $companyService, DailyStockService $dailyStockService, Helper $helper)
    {
        $this->companyService = $companyService;
        $this->dailyStockService = $dailyStockService;
        $this->helper = $helper;
    }

    // API:
    public function upload(UploadRequest $request, APIResponse $response) {
        // data pre-process
        $files = [];
        foreach ($request["stockData"] as $item) {
            array_push($files, $item["file"]);
        }

        // create daily stock records
        try {
            $this->dailyStockService->createRecordsByUploadFiles($files);
            $response->isSuccess();
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function accessByStockCode(AccessByStockCodeRequest $request, APIResponse $response)
    {
        // data pre-process
        $stockCodes = $request["stockCodes"];
        $startFrom = $this->helper->initRequestField($request, 'startFrom', '2000/1/1');
        $startFrom = $this->helper->jsTimestampOfTaiwan($startFrom);
        $endAt = $this->helper->initRequestField($request, 'endAt', '2200/12/31');
        $endAt = $this->helper->jsTimestampOfTaiwan($endAt);

        try {
            $dailyStocksCollection = $this->dailyStockService->searchAllByStockCodes($stockCodes, $startFrom, $endAt);
            $responseData = [];
            foreach ($dailyStocksCollection as $key => $dailyStockCollection) {
                $responseData[$key] = $this->dailyStockService->toDictionary($dailyStockCollection, 'timestamp');
            }
            $response->isSuccess()->setData($responseData);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }
}
