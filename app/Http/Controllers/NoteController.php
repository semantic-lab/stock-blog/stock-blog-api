<?php


namespace App\Http\Controllers;


use App\helpers\ConditionInfo;
use App\helpers\Helper;
use App\helpers\PageInfo;
use App\Http\Requests\Note\AccessAllNotesResult;
use App\Http\Requests\Note\AccessNoteByIdRequest;
use App\Http\Requests\Note\CreateNoteRequest;
use App\Http\Requests\Note\DeleteNoteRequest;
use App\Http\Requests\Note\UpdateNoteRequest;
use App\Http\Responses\APIResponse;
use App\Services\NoteService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class NoteController extends Controller
{
    private $noteService;
    private $userService;
    private $helper;

    public function __construct(NoteService $noteService, UserService $userService, Helper $helper)
    {
        $this->noteService = $noteService;
        $this->userService = $userService;
        $this->helper = $helper;
    }

    // API

    public function accessAllNotes(AccessAllNotesResult $request, PageInfo $pageInfo, APIResponse $response, ConditionInfo $conditionInfo)
    {
        // data pre-process
        $userInfo = $this->userService->userInfoInRequest($request);
        $conditions = ($userInfo['role'] !== "manager") ? [ ["user_id", "=", $userInfo["id"]] ] : [];
        $pageInfo->set([
            "at" => $request['pageAt'],
            "size" => $request['pageSize'],
        ]);
        $conditionInfo
            ->set($request, true)
            ->integrateConditions($conditions);

        // select notes
        try {
            $totalAmount = $this->noteService->count($conditions);
            $noteCollection = $this->noteService->search($conditions, $pageInfo);
            $noteCollection = $this->noteService->collectionInCamelCase($noteCollection);
            $response->isSuccess()->setData([
                "notes" => $noteCollection,
                "total" => $totalAmount
            ]);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function accessNoteById(AccessNoteByIdRequest $request, APIResponse $response)
    {
        // data pre-process
        $userInfo = $this->userService->userInfoInRequest($request);

        // select note
        try {
            $note = $this->noteService->searchById($request['noteId'], true, $userInfo);
            $note = $this->noteService->modelInCamelCase($note);
            $response->isSuccess()->setData($note);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function createNote(CreateNoteRequest $request, APIResponse $response)
    {
        // data pre-process
        $userInfo = $this->userService->userInfoInRequest($request);
        $timestamp = $this->helper->jsTimestampOfTaiwan($request["date"]);
        $newNoteData = [
            "title" => $request["title"],
            "content" => $request["content"],
            "user_id" => $userInfo["id"],
            "timestamp" => $timestamp,
        ];

        // create note
        try {
            $newNote = $this->noteService->create($newNoteData);
            $newNote = $this->noteService->modelInCamelCase($newNote);
            $response->isSuccess()->setData($newNote);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function updateNote(UpdateNoteRequest $request, APIResponse $response)
    {
        // data pre-process
        $userInfo = $this->userService->userInfoInRequest($request);
        $updateData = $request->only(["title", "content"]);

        // update
        try {
            $updatedNote = $this->noteService->update($request["noteId"], $updateData, true, $userInfo);
            $updatedNote = $this->noteService->modelInCamelCase($updatedNote);
            $response->isSuccess()->setData($updatedNote);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function deleteNote(DeleteNoteRequest $request, APIResponse $response)
    {
        // data pre-process
        $userInfo = $this->userService->userInfoInRequest($request);

        // delete
        try {
            $this->noteService->delete($request["noteId"], true, $userInfo);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }
}
