<?php


namespace App\Http\Controllers;


use App\helpers\Helper;
use App\Http\Requests\Tag\AccessDailyTadRequest;
use App\Http\Requests\Tag\AddNewDailyTagRequest;
use App\Http\Requests\Tag\RemoveDailyTagRequest;
use App\Http\Responses\APIResponse;
use App\Services\CompanyService;
use App\Services\UserDailyTagService;
use App\Services\UserService;

class UserDailyTagController extends Controller
{
    private $userDailyTagService;
    private $companyService;
    private $userService;
    private $helper;

    public function __construct(UserDailyTagService $userDailyTagService, CompanyService $companyService, UserService $userService, Helper $helper)
    {
        $this->userDailyTagService = $userDailyTagService;
        $this->companyService = $companyService;
        $this->userService = $userService;
        $this->helper = $helper;
    }

    // API:

    public function addNewDailyTag(AddNewDailyTagRequest $request, APIResponse $response)
    {
        // data pre-process
        $userInfo = $this->userService->userInfoInRequest($request);
        $userId = $userInfo['id'];
        $companyId = $this->companyService->companyIdInRequest($request);
        $timestamp = $request["timestamp"];
        $tagInfo = [
            "user_id" => $userId,
            "company_id" => $companyId,
            "timestamp" => $timestamp,
            "tagName" => $request["tagName"]
        ];

        // create new records
        try {
            $newTag = $this->userDailyTagService->createDailyRecord($tagInfo);
            $newTag = $this->userDailyTagService->modelInCamelCase($newTag);
            $response->isSuccess()->setData([ $newTag["timestamp"] => $newTag ]);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function removeDailyTag(RemoveDailyTagRequest $request, APIResponse $response)
    {
        // data pre-process
        $userInfo = $this->userService->userInfoInRequest($request);

        try {
            $this->userDailyTagService->delete($request["dailyTagId"], true, $userInfo);
            $response->isSuccess();
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function accessDailyTag(AccessDailyTadRequest $request, APIResponse $response)
    {
        // data pre-process
        $userInfo = $this->userService->userInfoInRequest($request);
        $userId = $userInfo["id"];
        $companyId = $this->companyService->companyIdInRequest($request);
        $startFrom = $this->helper->initRequestField($request, 'startFrom', '2000/1/1');
        $startFrom = $this->helper->jsTimestampOfTaiwan($startFrom);
        $endAt = $this->helper->initRequestField($request, 'endAt', '2200/12/31');
        $endAt = $this->helper->jsTimestampOfTaiwan($endAt);

        // select
        try {
            $userDailyTagCollection = $this->userDailyTagService->search([
                ["user_id", "=", $userId],
                ["company_id", "=", $companyId],
                ["timestamp", ">=", $startFrom],
                ["timestamp", "<=", $endAt]
            ]);
            $userDailyTagsGroupByTimestamp = $this->userDailyTagService
                ->toDictionary($userDailyTagCollection, "timestamp", true);
            $response->isSuccess()->setData($userDailyTagsGroupByTimestamp);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }
}
