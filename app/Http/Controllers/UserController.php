<?php


namespace App\Http\Controllers;


use App\helpers\ConditionInfo;
use App\helpers\ExceptionHelper;
use App\helpers\Helper;
use App\helpers\PageInfo;
use App\helpers\ResponseCode;
use App\Http\Requests\User\AccessUserRequest;
use App\Http\Requests\User\AccessUsersRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\LogoutRequest;
use App\Http\Requests\User\RemoveUserRequest;
use App\Http\Requests\User\UpdatePasswordRequest;
use App\Http\Requests\User\UpdateUserImageRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\Tag;
use App\Http\Responses\APIResponse;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $userService;
    private $helper;
    private $exceptionHelper;

    public function __construct(UserService $userService, Helper $helper, ExceptionHelper $exceptionHelper)
    {
        $this->userService = $userService;
        $this->helper = $helper;
        $this->exceptionHelper = $exceptionHelper;
    }

    // API

    public function login(LoginRequest $request, APIResponse $response)
    {
        // data pre-process
        $userData = $request->only([ 'email', 'password' ]);

        try {
            $token = auth()->attempt($userData);
            if ($token) {
                $response->isSuccess()->setData([ "token" => $token ]);
            } else {
                throw $this->exceptionHelper->getExceptionByName("loginError");
            }
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function accessUser(AccessUserRequest $request, APIResponse $response)
    {
        $userInfo = $this->userService->userInfoInRequest($request);
        $userId = $userInfo["id"];

        try {
            $user = $this->userService->searchById($userId);
            $user = $this->userService->modelInCamelCase($user);
            $response->isSuccess()->setData($user);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function accessUsers(AccessUsersRequest $request, PageInfo $pageInfo, APIResponse $response, ConditionInfo $conditionInfo)
    {
        // data pre-process
        $pageInfo->set([
            "at" => $request['pageAt'],
            "size" => $request['pageSize'],
        ]);
        $conditions = [];
        $conditionInfo
            ->set($request, true)
            ->integrateConditions($conditions);

        // select users
        try {
            $totalAmount = $this->userService->count();
            $userCollection = $this->userService->search($conditions, $pageInfo);
            $userCollection = $this->userService->collectionInCamelCase($userCollection);
            $response->isSuccess()->setData([
                "users" => $userCollection,
                "total" => $totalAmount,
            ]);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function createUser(CreateUserRequest $request, APIResponse $response)
    {
        $this->create($request, 2, $response);
        return $response->toJSONResponse();
    }

    public function createManager(CreateUserRequest $request, APIResponse $response) {
        $this->create($request, 1, $response);
        return $response->toJSONResponse();
    }

    public function updateUser(UpdateUserRequest $request, APIResponse $response)
    {
        $userInfo = $this->userService->userInfoInRequest($request);
        $userId = $userInfo["id"];
        $userData = $request->only([ 'name', 'email', 'phone' ]);

        try {
            $user = $this->userService->update($userId, $userData);
            $user = $this->userService->modelInCamelCase($user);
            $response->isSuccess()->setData($user);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function updateUserImage(UpdateUserImageRequest $request, APIResponse $response)
    {
        $userInfo = $this->userService->userInfoInRequest($request);
        $userId = $userInfo["id"];
        $file = $request->file("image");

        try {
            $user = $this->userService->saveUserImage($userId, $file);
            $user = $this->userService->modelInCamelCase($user);
            $response->isSuccess()->setData($user);
        } catch(\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function updatePassword(UpdatePasswordRequest $request, APIResponse $response)
    {
        $userInfo = $this->userService->userInfoInRequest($request);
        $originPassword = $request['originPassword'];
        $newPassword = $request['newPassword'];

        try {
            $user = $this->userService->updateUserPassword($userInfo, $originPassword, $newPassword);
            $user = $this->userService->modelInCamelCase($user);
            $response->isSuccess()->setData($user);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function removeUser(RemoveUserRequest $request, APIResponse $response)
    {
        $userInfo = $this->userService->userInfoInRequest($request);
        $email = $request['email'];
        $password = $request['password'];

        try {
            $this->userService->deleteUser($userInfo, $email, $password);
            auth()->logout();
            $response->isSuccess();
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
    }

    public function logout(LogoutRequest $request, APIResponse $response) {
        auth()->logout();
        return $response->isSuccess()->toJSONResponse();
    }

    public function authFailed() {
        return (new APIResponse())
                ->isFailed()->setCode(ResponseCode::AUTH_FAILED)
                ->setMessage("Unauthorized")->toJSONResponse(401);
    }

    public function roleFailed() {
        return (new APIResponse())
            ->isFailed()->setCode(ResponseCode::ROLE_FAILED)
            ->setMessage("Role permission denied")->toJSONResponse();
    }

    // private
    private function create(CreateUserRequest $request, $type, APIResponse &$response)
    {
        $newUserData = $request->only([ 'name', 'email', 'phone', 'password' ]);
        $newUserData['password'] = Hash::make($newUserData['password']);
        $newUserData['type'] = $type;

        try {
            $newUser = $this->userService->create($newUserData);
            $newUser = $this->userService->modelInCamelCase($newUser);
            $response->isSuccess()->setData($newUser);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
    }
}
