<?php


namespace App\Http\Controllers;


use App\helpers\ConditionInfo;
use App\helpers\Helper;
use App\helpers\PageInfo;
use App\Http\Requests\News\AccessDailyCompanyNewsRequest;
use App\Http\Requests\News\AccessDailyNewsRequest;
use App\Http\Requests\News\ClassifyNewsByTimestampRequest;
use App\Http\Requests\News\ClassifyNewsRequest;
use App\Http\Requests\News\CrawledNewsRequest;
use App\Http\Responses\APIResponse;
use App\Services\CompanyNewsService;
use App\Services\CompanyService;
use App\Services\NewsService;
use Carbon\Carbon;

class CompanyNewsController extends Controller
{
    private $companyNewsService;
    private $newsService;
    private $companyService;
    private $helper;

    public function __construct(CompanyNewsService $companyNewsService, NewsService $newsService, CompanyService $companyService, Helper $helper)
    {
        $this->companyNewsService = $companyNewsService;
        $this->newsService = $newsService;
        $this->companyService = $companyService;
        $this->helper = $helper;
    }

    // API:

    public function accessDailyNews(AccessDailyNewsRequest $request, APIResponse $response, PageInfo $pageInfo, ConditionInfo $conditionInfo)
    {
        // data
        $conditions = [ ["timestamp", "=", $request["timestamp"] ] ];
        $pageInfo->set([
            "at" => $request["pageAt"],
            "size" => $request["pageSize"]
        ]);
        $conditionInfo
            ->set($request, true)
            ->integrateConditions($conditions);

        // select news
        try {
            $totalNewsWithConditionsAmount = $this->newsService->count($conditions);
            $companyNewsCollection = $this->newsService->search($conditions, $pageInfo);
            $companyNewsCollection = $this->newsService->collectionInCamelCase($companyNewsCollection);
            $response->isSuccess()->setData([ "total" => $totalNewsWithConditionsAmount, "news" => $companyNewsCollection ]);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function accessDailyCompanyNews(AccessDailyCompanyNewsRequest $request, APIResponse $response, ConditionInfo $conditionInfo)
    {
        // data pre-process
        $companyId = $this->companyService->companyIdInRequest($request);
        $conditions = [ ['company_id', '=', $companyId] ];
        $timestamp = $request['timestamp'];
        if ($timestamp) {
            array_push($conditions, ['timestamp', '=', $timestamp]);
        }
       $conditionInfo
           ->set($request, true)
           ->integrateConditions($conditions);

        // select company daily news
        try {
            $companyCollection = $this->companyNewsService->search($conditions);
            $companiesGroupByTimestamp = $this->companyNewsService->toDictionary($companyCollection, "timestamp", true);
            $response->isSuccess()->setData($companiesGroupByTimestamp);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function crawledNews(CrawledNewsRequest $request, APIResponse $response)
    {
        // data pre-process
        $crawlingType = env('NEWS_TYPE');
        $crawlingResult = [
            "currentPage" => 0,
            "nextPage" => 0,
            "totalPage" => 0,
            "finished" => false,
            "news" => [],
        ];
        switch ($crawlingType) {
            case "cnyes":
                $this->newsService->crawlingCnyesNews($request, $crawlingResult);
                break;
        }

        // create crawled news
        try {
            $this->newsService->createNews($crawlingResult);
            $responseData = [
                "finished" => $crawlingResult["finished"],
                "totalPage" => $crawlingResult["totalPage"],
                "nextPage" => $crawlingResult["nextPage"],
            ];
            $response->isSuccess()->setData($responseData);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function classifyNews(ClassifyNewsRequest $request, APIResponse $response)
    {
        $newsIdList = [];
        foreach ($request['newsIdList'] as $newsId) {
            array_push($newsIdList, $newsId['id']);
        }

        try {
            $this->newsService->classify($newsIdList);
            $response->isSuccess();
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function classifyNewsByTimestamp(ClassifyNewsByTimestampRequest $request, APIResponse $response)
    {
        $limitSecond = 1 * 60 * 5; // 5 mins
        set_time_limit($limitSecond);
        $startAt = $request['startAt'];
        $endAt = $request['endAt'];
        $conditions = [
            [ 'timestamp', '>=', $startAt ],
            [ 'timestamp', '<=', $endAt ],
        ];

        try {
            $newsIdList = [];
            $newsCollection = $this->newsService->search($conditions, null, null, ['id']);
            foreach ($newsCollection as $news) {
                array_push($newsIdList, $news['id']);
            }
            $this->newsService->classify($newsIdList);
            $response->isSuccess();
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }
}
