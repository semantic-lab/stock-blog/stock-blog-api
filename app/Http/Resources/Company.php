<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $keywordsToShow = [];
        foreach ($this->keywords as $key => $keyword)
        {
            $keywordsToShow[$key] = [
                "id" => $keyword->id,
                "name" => $keyword->name,
            ];
        }

        return [
            "id" => $this->id,
            "name" => $this->name,
            "stockCode" => $this->stockCode,
            "createdAt" => $this->createdAt,
            "updatedAt" => $this->updatedAt,
            "keywords" => $keywordsToShow,
        ];
    }
}
