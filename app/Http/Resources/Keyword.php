<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Keyword extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "company" => [
                "id" => $this->company->id,
                "name" => $this->company->name,
                "stockCode" => $this->company->stockData
            ],
        ];
    }
}
