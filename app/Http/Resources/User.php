<?php

namespace App\Http\Resources;

use App\helpers\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // image
        $imageUrl = ($this->image) ? "/image/{$this->id}/{$this->image}" : null;

        // type
        $type = null;
        switch ($this->type) {
            case 0:
                $type = "dev";
                break;
            case 1:
                $type = "manager";
                break;
            case 2:
                $type = "user";
                break;
        }

        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "phone" => $this->phone,
            "image" => $imageUrl,
            "type" => $type,
            "createdAt" => $this->created_at->format("Y-m-d"),
            "updatedAt" => $this->updated_at->format("Y-m-d"),
            "deletedAt" => (is_null($this->deleted_at)) ? $this->deleted_at : $this->deleted_at->format("Y-m-d"),
            "isDeleted" => !is_null($this->deleted_at),
        ];
    }
}
