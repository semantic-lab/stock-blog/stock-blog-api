<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserDailyTag extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "timestamp" => $this->timestamp,
            "userId" => $this->userId,
            "companyId" => $this->companyId,
            "tagId" => $this->tag->id,
            "tagName" => $this->tag->name,
        ];
    }
}
