<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DailyStock extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "companyId" => $this->company_id,
            "timestamp" => $this->timestamp,
            "date" => $this->date,
            "openingPrice" => $this->opening_price,
            "dailyMaxPrice" => $this->daily_max_price,
            "dailyMinPrice" => $this->daily_min_price,
            "closingPrice" => $this->closing_price,
            "volume" => $this->volume,
            "avgPrice1" => $this->avg_price1,
            "avgPrice5" => $this->avg_price5,
            "avgPrice10" => $this->avg_price10,
            "avgPrice20" => $this->avg_price20,
            "avgPrice60" => $this->avg_price60,
            "avgPrice120" => $this->avg_price120,
            "avgPrice240" => $this->avg_price240,
            "avgVolume5" => $this->avg_volume5,
            "avgVolume20" => $this->avg_volume20,
            "avgVolume60" => $this->avg_volume60,
            "avgVolume120" => $this->avg_volume120,
            "macdBar" => $this->macd_bar,
            "ks933" => $this->ks933,
            "ds933" => $this->ds933,
            "priceChangeValue" => $this->price_change_value,
            "priceChangeType" => $this->price_change_type,
            "candleStickColor" => $this->candle_stick_color,
        ];
    }
}
