<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyNews extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $news = $this->news;

        return [
            "id" => $this->id,
            "timestamp" => $this->timestamp,
            "company_id" => $this->company_id,
            "newsId" => $news->id,
            "url" => $news->url,
            "title" => $news->title,
        ];
    }
}
