<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Note extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "timestamp" => $this->timestamp,
            "userId" => $this->userId,
            "title" => $this->title,
            "content" => $this->content,
            "deletedAt" => $this->deleted_at,
            "createdAt" => $this->create_at,
            "updatedAt" => $this->create_at,
        ];
    }
}
