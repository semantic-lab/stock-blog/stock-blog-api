<?php


namespace App\Repositories;


use App\Models\Tag;

class TagRepository extends BaseRepository
{

    protected function modelClass()
    {
        return Tag::class;
    }

    public function searchModelByName($name, $exceptionOnNull = true)
    {
        $modelInstance = $this->modelClass::where([ ["name", "=", $name] ])->first();
        if ($modelInstance || !$exceptionOnNull) {
            return $modelInstance;
        }
        throw $this->exceptionHelper->getExceptionByName('selectNoResultError');
    }
}
