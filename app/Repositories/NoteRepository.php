<?php


namespace App\Repositories;


use App\Models\Note;

class NoteRepository extends BaseRepository
{

    protected function modelClass()
    {
        return Note::class;
    }
}
