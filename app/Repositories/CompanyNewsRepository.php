<?php


namespace App\Repositories;


use App\Models\CompanyNews;

class CompanyNewsRepository extends BaseRepository
{

    protected function modelClass()
    {
        return CompanyNews::class;
    }
}
