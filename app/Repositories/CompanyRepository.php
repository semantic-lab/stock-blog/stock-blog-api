<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/13
 * Time: 下午 04:10
 */

namespace App\Repositories;


use App\Models\Company;

class CompanyRepository extends BaseRepository
{
    protected function modelClass()
    {
        return Company::class;
    }

    public function searchCompanyByCode($stockCode, $exceptionOnNull = true)
    {
        $companyInstance = $this->modelClass::where('stock_code', $stockCode)->first();
        if ($companyInstance || !$exceptionOnNull) {
            return $companyInstance;
        }
        throw $this->exceptionHelper->getExceptionByName('selectNoResultError');
    }
}
