<?php


namespace App\Repositories;


use App\helpers\ExceptionHelper;
use App\helpers\OrderInfo;
use App\helpers\PageInfo;
use App\Models\DailyStock;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements IRepository
{
    protected $app;
    protected $modelClass;
    protected $exceptionHelper;

    public function __construct(App $app, ExceptionHelper $exceptionHelper)
    {
        $this->app = $app;
        $this->modelClass = $this->modelClass();
        $this->exceptionHelper = $exceptionHelper;
    }

    protected abstract function modelClass();

    public function create(array $data)
    {
        $modelInstance = resolve($this->modelClass);
        $createSuccess = $this->saveModel($modelInstance, $data);
        if ($createSuccess) {
            return $modelInstance;
        }
        throw $this->exceptionHelper->getExceptionByName('createModelError');
    }

    public function update($target, array $data)
    {
        $modelInstance = $this->initModelInstance($target);
        $updatedSuccess = $this->saveModel($modelInstance, $data);
        if ($updatedSuccess) {
            return $modelInstance;
        }
        throw $this->exceptionHelper->getExceptionByName('updateModelError');
    }

    public function delete($target)
    {
        $modelInstance = $this->initModelInstance($target);
        $deleteSuccess = $this->modelClass::destroy($modelInstance->id);
        if (!$deleteSuccess) {
            throw $this->exceptionHelper->getExceptionByName('deleteModelError');
        }
    }

    public function searchModelById($id, $exceptionOnNull = true)
    {
        $modelInstance = $this->modelClass::find($id);
        if ($modelInstance || !$exceptionOnNull) {
            return $modelInstance;
        }
        throw $this->exceptionHelper->getExceptionByName('selectNoResultError');
    }

    public function searchModels(array $conditions = [], PageInfo $pageInfo = null, OrderInfo $orderInfo = null, array $columns = array('*'))
    {
        try {
            $query = $this->initQuery();
            $query = $this->addConditions($query, $conditions);

            if ($pageInfo) {
                $query->offset($pageInfo->offset)->limit($pageInfo->size);
            }

            if ($orderInfo) {
                $query->orderBy($orderInfo->column, $orderInfo->orientation);
            }

            return $query->get($columns);
        } catch (\Exception $exception) {
            $exceptionCode = $exception->getCode();
            $isColumnNotExistException = $exceptionCode === '42S22';
            if ($isColumnNotExistException) {
                throw $this->exceptionHelper->getExceptionByName('sqlColumnUndefinedError');
            }
            throw $exception;
        }
    }

    public function count(array $conditions = []) {
        $query = $this->initQuery();
        $query = $this->addConditions($query, $conditions);
        return $query->count();
    }

    protected function initModelInstance($target)
    {
        $isInstance = $target && $target instanceof $this->modelClass;
        $isId = $target && is_int($target);
        if ($isInstance) {
            return $target;
        } else if ($isId) {
            return $this->searchModelById($target, true);
        }
        throw $this->exceptionHelper->getExceptionByName('initModelError');
    }

    protected function saveModel(&$modelInstance, array $data)
    {
        if (isset($modelInstance)) {
            foreach($data as $key => $value) {
                if (isset($value)) {
                    $modelInstance[$key] = $value;
                }
            }
            return $modelInstance->save();
        }
        return false;
    }

    protected function initQuery() {
        try {
            return $this->modelClass::withTrashed();
        } catch (\Exception $ex) {
            return $this->app->make($this->modelClass);
        }
    }

    protected function addConditions($query, array $conditions)
    {
        $whereConditions = [];
        $withConditions = [];

        // classify conditions with different key
        foreach ($conditions as $key => $condition) {
            $isWithConditions = $key === 'with';
            if ($isWithConditions) {
                $withConditions =$condition;
            } else {
                array_push($whereConditions, $condition);
            }
        }

        // bind conditions to eloquent where()
        $query = $query->where($whereConditions);

        // bind conditions to several eloquent whereHas() (with())
        foreach ($withConditions as $foreignProp => $foreignConditions) {
            $query = $query->whereHas($foreignProp, function ($subQuery) use ($foreignConditions) {
                $subQuery->where($foreignConditions);
            });
        }

        return $query;
    }
}
