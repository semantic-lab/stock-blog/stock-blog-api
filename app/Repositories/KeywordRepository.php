<?php


namespace App\Repositories;


use App\Models\Keyword;

class KeywordRepository extends BaseRepository
{

    protected function modelClass()
    {
        return Keyword::class;
    }
}
