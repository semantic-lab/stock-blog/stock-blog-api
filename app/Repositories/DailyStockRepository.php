<?php


namespace App\Repositories;


use App\helpers\OrderInfo;
use App\helpers\PageInfo;
use App\Models\DailyStock;

class DailyStockRepository extends BaseRepository
{
    protected function modelClass()
    {
        return DailyStock::class;
    }

    public function searchModelsByCode($stockCode, array $conditions = [], PageInfo $pageInfo = null, OrderInfo $orderInfo = null, array $columns = array('*'))
    {
        $stockCodeCondition = [ ["stock_code", "=", $stockCode] ];
        array_push($conditions, $stockCodeCondition);

        return $this->searchModels($conditions, $pageInfo, $orderInfo, $columns);
    }
}
