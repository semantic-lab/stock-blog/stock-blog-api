<?php


namespace App\Repositories;


use App\Models\News;

class NewsRepository extends BaseRepository
{

    protected function modelClass()
    {
        return News::class;
    }
}
