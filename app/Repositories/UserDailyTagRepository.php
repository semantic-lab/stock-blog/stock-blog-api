<?php


namespace App\Repositories;


use App\Models\UserDailyTag;

class UserDailyTagRepository extends BaseRepository
{

    protected function modelClass()
    {
        return UserDailyTag::class;
    }
}
