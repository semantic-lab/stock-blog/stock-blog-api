<?php


namespace App\Services;


use App\helpers\ExceptionHelper;
use App\Http\Resources\Keyword;
use App\Repositories\KeywordRepository;

class KeywordService extends BaseService
{
    public function __construct(KeywordRepository $keywordRepository, ExceptionHelper $exceptionHelper)
    {
        parent::__construct($keywordRepository, Keyword::class, $exceptionHelper);
    }

    public function hasPermission($userInfo, $targetId)
    {
        return true;
    }
}
