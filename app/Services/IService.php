<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/14
 * Time: 下午 05:14
 */

namespace App\Services;


use App\helpers\OrderInfo;
use App\helpers\PageInfo;
use Illuminate\Support\Collection;

interface IService
{
    public function create(array $data);

    public function update($target, array $data, $checkPermission = false, $userInfo = []);

    public function delete($target, $checkPermission = false, $userInfo = []);

    public function searchById($id, $checkPermission = false, $userInfo = [], $exceptionOnNull = true);

    public function search(array $conditions = [], PageInfo $pageInfo = null, OrderInfo $orderInfo = null, array $columns = ['*']);

    public function count(array $conditions = []);

    public function modelInCamelCase($model);

    public function collectionInCamelCase(Collection $modelCollection);

    public function toDictionary(Collection $modelCollection, string $property);

    public function hasPermission($userInfo, $data);
}
