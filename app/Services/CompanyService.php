<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/13
 * Time: 下午 04:30
 */

namespace App\Services;


use App\helpers\ExceptionHelper;
use App\Http\Resources\Company;
use App\Repositories\CompanyRepository;
use Illuminate\Foundation\Http\FormRequest;

class CompanyService extends BaseService
{
    public function __construct(CompanyRepository $companyRepository, ExceptionHelper $exceptionHelper)
    {
        parent::__construct($companyRepository, Company::class, $exceptionHelper);
    }

    public function companyIdInRequest(FormRequest $request)
    {
        $result = 0;
        $type = $request['type'];
        $hasCompanyId = $type === "companyId" && !is_null($request['companyId']);
        $hasStockCode = $type === "stockCode" && !is_null($request['stockCode']);
        if ($hasCompanyId) {
            $result = $request['companyId'];
        } else if ($hasStockCode) {
            $company = $this->repository->searchCompanyByCode($request['stockCode']);
            if ($company) {
                $result = $company->id;
            }
        }
        return $result;
    }

    public function hasPermission($userInfo, $targetId)
    {
        return true;
    }
}
