<?php


namespace App\Services;


use App\helpers\ExceptionHelper;
use App\Http\Resources\UserDailyTag;
use App\Repositories\TagRepository;
use App\Repositories\UserDailyTagRepository;

class UserDailyTagService extends BaseService
{
    private $tagRepository;

    public function __construct(UserDailyTagRepository $userDailyTagRepository, TagRepository $tagRepository, ExceptionHelper $exceptionHelper)
    {
        parent::__construct($userDailyTagRepository, UserDailyTag::class, $exceptionHelper);
        $this->tagRepository = $tagRepository;
    }

    public function createDailyRecord(array $tagInfo)
    {
        $tagName = $tagInfo["tagName"];
        // create tag if not exist
        try {
            $tagInfo["tag_id"] = ($this->tagRepository->searchModelByName($tagName))->id;
        } catch (\Exception $exception) {
            $tagInfo["tag_id"] = ($this->tagRepository->create([ "name" => $tagName ]))->id;
        }
        unset($tagInfo['tagName']);
        // create new tag
        return $this->create($tagInfo);
    }

    public function deleteDailyRecord($userInfo, $tagId)
    {
        $dailyTag = $this->repository->searchModelById($tagId);
        $isManger = $userInfo['role'] === "manager" || $userInfo['role'] === "proxy";
        $isAuthor = $dailyTag->user_id === $userInfo["id"];
        if ($isManger || $isAuthor) {
            $this->delete($tagId);
        }
        throw $this->exceptionHelper->getExceptionByName("actionWithoutPermissionError");
    }

    public function hasPermission($userInfo, $targetId)
    {
        $dailyTag = $this->repository->searchModelById($targetId);
        $isManger = $userInfo['role'] === "manager" || $userInfo['role'] === "proxy";
        $isAuthor = $dailyTag->user_id === $userInfo["id"];
        return ($isManger || $isAuthor);
    }
}
