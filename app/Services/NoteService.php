<?php


namespace App\Services;


use App\helpers\ExceptionHelper;
use App\Http\Resources\Note;
use App\Repositories\NoteRepository;

class NoteService extends BaseService
{
    protected $exceptionHelper;

    public function __construct(NoteRepository $noteRepository, ExceptionHelper $exceptionHelper)
    {
        parent::__construct($noteRepository, Note::class, $exceptionHelper);
        $this->exceptionHelper = $exceptionHelper;
    }

    public function hasPermission($userInfo, $targetId)
    {
        $note = $this->repository->searchModelById($targetId);
        $isManger = $userInfo['role'] === "manager" || $userInfo['role'] === "proxy";
        $isAuthor = $note->user_id === $userInfo['id'];
        return ($isManger || $isAuthor);
    }
}
