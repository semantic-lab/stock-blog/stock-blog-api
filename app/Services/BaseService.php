<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/14
 * Time: 下午 05:22
 */

namespace App\Services;


use App\helpers\ExceptionHelper;
use App\helpers\OrderInfo;
use App\helpers\PageInfo;
use App\Repositories\IRepository;
use Illuminate\Support\Collection;

abstract class BaseService implements IService
{
    protected $repository;
    protected $resourceClass;
    protected $exceptionHelper;

    public function __construct(IRepository $repository, $resourceClass, ExceptionHelper $exceptionHelper)
    {
        $this->repository = $repository;
        $this->resourceClass = $resourceClass;
        $this->exceptionHelper = $exceptionHelper;
    }

    public function create(array $data)
    {
        return $this->repository->create($data);
    }

    public function update($id, array $data, $checkPermission = false, $userInfo = [])
    {
        $hasPermission = $this->hasPermission($userInfo, $id);
        if ($hasPermission || !$checkPermission) {
            return $this->repository->update($id, $data);
        }
        throw $this->exceptionHelper->getExceptionByName("actionWithoutPermissionError");
    }

    public function delete($id, $checkPermission = false, $userInfo = [])
    {
        $hasPermission = $this->hasPermission($userInfo, $id);
        if ($hasPermission || !$checkPermission) {
            $this->repository->delete($id);
        } else {
            throw $this->exceptionHelper->getExceptionByName("actionWithoutPermissionError");
        }
    }

    public function searchById($id, $checkPermission = false, $userInfo = [], $exceptionOnNull = true)
    {
        $hasPermission = $this->hasPermission($userInfo, $id);
        if ($hasPermission || !$checkPermission) {
            return $this->repository->searchModelById($id, $exceptionOnNull);
        }
        throw $this->exceptionHelper->getExceptionByName("actionWithoutPermissionError");
    }

    public function search(array $conditions = [], PageInfo $pageInfo = null, OrderInfo $orderInfo = null, array $columns = ['*'])
    {
        return $this->repository->searchModels($conditions, $pageInfo, $orderInfo, $columns);
    }

    public function count(array $conditions = [])
    {
        return $this->repository->count($conditions);
    }

    public function modelInCamelCase($model)
    {
        if ($model) {
            return new $this->resourceClass($model);
        } else {
            throw $this->exceptionHelper->getExceptionByName('transModelInCamelCaseError');
        }
    }

    public function collectionInCamelCase(Collection $modelCollection)
    {
        try {
            return $this->resourceClass::collection($modelCollection);
        } catch (\Exception $ex) {
            throw $this->exceptionHelper->getExceptionByName('transModelCollectionInCamelCaseError');
        }
    }

    public function toDictionary(Collection $modelCollection, string $property = 'id', bool $groupInArray = false)
    {
        $result = [];
        foreach ($modelCollection as $model) {
            $resultKey = $model[$property];
            $isEmpty = empty($result[$resultKey]);
            $value = $this->modelInCamelCase($model);
            if ($groupInArray) {
                if ($isEmpty) {
                    $result[$resultKey] = [];
                }
                array_push($result[$resultKey], $value);
            } else {
                $result[$resultKey] = $value;
            }
        }
        return $result;
    }

    public abstract function hasPermission($userInfo, $targetId);
}
