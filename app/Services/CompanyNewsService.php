<?php


namespace App\Services;


use App\helpers\ExceptionHelper;
use App\Http\Resources\CompanyNews;
use App\Repositories\CompanyNewsRepository;

class CompanyNewsService extends BaseService
{
    public function __construct(CompanyNewsRepository $companyNewsRepository, ExceptionHelper $exceptionHelper)
    {
        parent::__construct($companyNewsRepository, CompanyNews::class, $exceptionHelper);
    }

    public function hasPermission($userInfo, $targetId)
    {
        return true;
    }
}
