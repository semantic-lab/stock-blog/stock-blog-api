<?php


namespace App\Services;


use App\helpers\ExceptionHelper;
use App\Http\Resources\News;
use App\Repositories\CompanyNewsRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\NewsRepository;
use Illuminate\Foundation\Http\FormRequest;

class NewsService extends BaseService
{
    private $companyRepository;
    private $companyNewsRepository;

    public function __construct(NewsRepository $newsRepository, CompanyRepository $companyRepository,
                                CompanyNewsRepository $companyNewsRepository, ExceptionHelper $exceptionHelper)
    {
        parent::__construct($newsRepository, News::class, $exceptionHelper);
        $this->companyRepository = $companyRepository;
        $this->companyNewsRepository = $companyNewsRepository;
    }

    public function crawlingCnyesNews(FormRequest $request, &$result)
    {
        // api data pre-process
        $apiURL = str_replace("_START_AT_", $request['startAt'], env('CNYES_NEWS_API'));
        $apiURL = str_replace("_END_AT_", $request['endAt'], $apiURL);
        $apiURL = str_replace("_PAGE_", $request['page'], $apiURL);
        // api result
        $crawlingResult = json_decode(file_get_contents($apiURL), true);
        $result["currentPage"] = $crawlingResult["items"]["current_page"];
        $result["nextPage"] = $result["currentPage"] + 1;
        $result["totalPage"] = $crawlingResult["items"]["last_page"];
        $result["finished"] = $result["currentPage"] === $result["totalPage"];
        // post-process for general format
        foreach ($crawlingResult["items"]["data"] as $news) {
            $newsModelData = [
                "timestamp" => $request['startAt'] * 1000,
                "url" => str_replace("_ID_", $news["newsId"], env('CNYES_NEWS_ARTICAL_URL')),
                "title" => $news["title"],
            ];
            array_push($result['news'], $newsModelData);
        }
    }

    public function createNews($crawlingResult)
    {
        foreach ($crawlingResult['news'] as $newsData) {
            try {
                $this->create($newsData);
            } catch (\Exception $exception) {
                // ignore
            }
        }
    }

    public function classify($newsIdList)
    {
        $allCompanies = $this->companyRepository->searchModels();
        foreach ($newsIdList as $newsId) {
            try {
                $news = $this->repository->searchModelById($newsId);
                if ($news) {
                    $isClassified = false;
                    foreach ($allCompanies as $company) {
                        foreach ($company['keywords'] as $keyword) {
                            $titleIncludesKeywords = strpos($news['title'], $keyword['name']) !== false;
                            if ($titleIncludesKeywords) {
                                $companyNewsRecord = [
                                    'timestamp' => $news['timestamp'],
                                    'company_id' => $company['id'],
                                    'news_id' => $newsId,
                                ];
                                try {
                                    $this->companyNewsRepository->create($companyNewsRecord);
                                } catch (\Exception $exception) {
                                    // ignore
                                }
                                $isClassified = true;
                                break;
                            }
                        }
                    }

                    if ($isClassified) {
                        $this->repository->update($newsId, [ 'classify' => true ]);
                    }
                }
            } catch (\Exception $exception) {
                // ignore
            }
        }
    }


    public function hasPermission($userInfo, $targetId)
    {
        return true;
    }
}
