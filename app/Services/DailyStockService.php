<?php


namespace App\Services;


use App\helpers\ExceptionHelper;
use App\helpers\Helper;
use App\helpers\OrderInfo;
use App\Http\Resources\DailyStock;
use App\Repositories\CompanyRepository;
use App\Repositories\DailyStockRepository;
use Illuminate\Support\Facades\Storage;

class DailyStockService extends BaseService
{
    private $companyRepository;
    private $helper;

    public function __construct(DailyStockRepository $dailyStockRepository, CompanyRepository $companyRepository,
                                ExceptionHelper $exceptionHelper, Helper $helper)
    {
        parent::__construct($dailyStockRepository, DailyStock::class, $exceptionHelper);
        $this->companyRepository = $companyRepository;
        $this->helper = $helper;
    }

    public function createRecordsByUploadFiles(array $files)
    {
        // save into storage (return path, stockCode)
        $fileInfoList = $this->saveFileInStorage($files);

        foreach ($fileInfoList as $fileInfo) {
            $filePath = $fileInfo['filePath'];
            $company = $fileInfo['company'];

            // parsing data and save
            $this->parseAndSaveStockData($filePath, $company->id);

            // remove file
            Storage::delete($fileInfo['filePath']);
        }
    }

    public function searchAllByStockCodes(array $stockCodes, $startFrom, $endAt)
    {
        $result = [];
        $baseConditions = [
            [ "timestamp", ">=", $startFrom ],
            [ "timestamp", "<=", $endAt ]
        ];
        $orderInfo = new OrderInfo();
        $orderInfo->set([
            "column" => "timestamp",
            "orientation" => "asc",
        ]);

        foreach($stockCodes as $stockCode) {
            try {
                $company = $this->companyRepository->searchCompanyByCode($stockCode);
                $conditions = [];
                array_push($conditions, [ "company_id", "=", $company->id ], $baseConditions[0], $baseConditions[1]);
                $result[$stockCode] = $this->repository->searchModels($conditions, null, $orderInfo);
            } catch (\Exception $exception) {
                $result[$stockCode] = null;
            }
        }
        return $result;
    }

    public function hasPermission($userInfo, $targetId)
    {
        return true;
    }


    // private

    private function saveFileInStorage(array $files)
    {
        $result = [];
        foreach ($files as $file) {
            try {
                $fileName = $file->getClientOriginalName();
                $stockCode = str_replace(".txt", "", $fileName);
                $company = null;
                try {
                    $company = $this->companyRepository->searchCompanyByCode($stockCode);
                } catch (\Exception $exception) {
                    $company = $this->companyRepository->create([
                        "name" => $stockCode,
                        "stock_code" => $stockCode
                    ]);
                }
                $companyData = [
                    "filePath" => "/stock-data/{$fileName}",
                    "fileName" => $fileName,
                    "stockCode" => $stockCode,
                    "company" => $company,
                ];
                array_push($result, $companyData);
                $file->storeAs("/stock-data", $fileName);
            } catch (\Exception $exception) {
                // ignore
            }
        }
        return $result;
    }

    private function parseAndSaveStockData($filePath, $companyId)
    {
        $fields = [
            "date", "opening_price", "daily_max_price", "daily_min_price", "closing_price",
            "volume", "avg_price1", "avg_price5", "avg_price10", "avg_price20",
            "avg_price60", "avg_price120", "avg_price240", "avg_volume5", "avg_volume20",
            "avg_volume60", "avg_volume120", "macd_bar", "ks933", "ds933"
        ];
        $fileContent = Storage::get($filePath);
        $dailyRecords = explode("\r\n", $fileContent);
        // remove table header
        \array_splice($dailyRecords, 0, 1);
        $dailyRecords = array_reverse($dailyRecords);
        foreach ($dailyRecords as $index => $dailyRecord) {
            $fieldValues = explode("\t", $dailyRecord);
            $haveEnoughField = count($fieldValues) === 20;
            if ($haveEnoughField) {
                $yesterdayClosingPrice = 0;
                $indexOfYesterday = $index + 1;
                $hasYesterdayRecord = $indexOfYesterday < count($dailyRecords);
                if ($hasYesterdayRecord) {
                    $yesterdayFieldValues = explode("\t", $dailyRecords[$indexOfYesterday]);
                    $yesterdayClosingPrice = $yesterdayFieldValues[4];
                }


                $conditions = [ [ "date", "=", $fieldValues[0] ], [ "company_id", "=", $companyId] ];
                $recordIsExist = $this->repository->count($conditions) > 0;
                if (!$recordIsExist) {
                    $newDailyStock = [
                        "timestamp" => $this->helper->jsTimestampOfTaiwan($fieldValues[0]),
                        "company_id" => $companyId,
                        "price_change_value" => $fieldValues[4] - $yesterdayClosingPrice,
                        "price_change_type" => $this->priceChangeType($yesterdayClosingPrice, $fieldValues[4]),
                        "candle_stick_color" => $this->candleStickColor($fieldValues[1], $fieldValues[4]),
                    ];
                    $fieldIndex = 0;
                    foreach($fields as $field) {
                        $newDailyStock[$field] = str_replace("↑", "", $fieldValues[$fieldIndex]);
                        $newDailyStock[$field] = str_replace("↓", "", $newDailyStock[$field]);
                        $newDailyStock[$field] = str_replace(" ", "", $newDailyStock[$field]);
                        $fieldIndex++;
                    }
                    $this->repository->create($newDailyStock);
                } else {
                    break;
                }
            }
        }
    }

    private function priceChangeType($yesterdayClosingPrice, $todayClosingPrice)
    {
        $advance = $yesterdayClosingPrice < $todayClosingPrice;
        $decline = $yesterdayClosingPrice > $todayClosingPrice;
        if ($advance) {
            return "advance";
        } else if ($decline) {
            return "decline";
        }
        return "constant";
    }

    private function candleStickColor($openingPrice, $closingPrice)
    {
        $advance = $openingPrice < $closingPrice;
        $decline = $openingPrice > $closingPrice;
        if ($advance) {
            return "#ff6666";
        } else if ($decline) {
            return "#00C48D";
        }
        return "#9c9c9c";
    }
}
